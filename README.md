# Go Music

A music player/playlist manager written in Go.

Allows for shuffle, playlist, or dynamic playlist, i.e. build queue on the fly.

### Features:

- search songs
- search artists
- list
- shuffle
- sort by artist

### Controls:

- pause
- restart
- skip

### Dependancies (debian):

- libasound2-dev
- libudev-dev

## TODOs:

### Part 1:

[ ] index songs at startup  
[ ] make trie to search  
[X] implement "Press (#) to add"  
[X] implement menu play  
[X] implement add to queue  
[ ] implement skip (next)  
[ ] implement view queue  
[ ] implement shuffle  
[ ] implement conf file to set music directory  

### Part 2:

[ ] implement sqlite db  
[ ] implement playlists  
    [ ] playlist CRUD  
    [ ] implement playlist shuffle  


NOTE: Playlists could use same queue features
