package main

import (
	"fmt"
	"github.com/gopxl/beep"
	"github.com/gopxl/beep/mp3"
	"github.com/gopxl/beep/speaker"
	"os"
	"strings"
	"time"
)

var speakerNotInit bool = true

func playSong(song string) {
	f, err := os.Open(song)
	if err != nil {
		fmt.Println("Cannot read music")
	}

	streamer, format, err := mp3.Decode(f)
	if err != nil {
		fmt.Println("Cannot play music")
		return
	}
	defer streamer.Close()

	if speakerNotInit {
		err = speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
		if err != nil {
			fmt.Println("Cannot init speaker")
			return
		}
		speakerNotInit = false
	}

	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))
	<-done
}

func listArtists(artists []string) {
	for index, artist := range artists {

		fmt.Println("(", index, ")", artist)

	}
}

func listAlbums(artistPath string) []string {
	contents, err := os.ReadDir(artistPath)
	albums := make([]string, 0)

	if err != nil {
		fmt.Println("Cannot read music directory")
	}

	for _, album := range contents {
		if album.IsDir() {
			albums = append(albums, album.Name())
		}
	}

	for index, album := range albums {

		fmt.Println("(", index, ")", album)

	}
	return albums
}

func listTracks(albumPath string) []string {
	contents, err := os.ReadDir(albumPath)
	tracks := make([]string, 0)

	if err != nil {
		fmt.Println("Cannot read music directory")
	}

	for _, track := range contents {
		if !track.IsDir() {
			tracks = append(tracks, track.Name())
		}
	}

	for index, track := range tracks {

		fmt.Println("(", index, ")", track)

	}
	return tracks
}

func enqueue(arr *[]string, song string) {
	*arr = append(*arr, song)
}

func dequeue(arr *[]string) string {
	if len(*arr) <= 0 {
		return ""
	}

	tmp := (*arr)[0]
	*arr = (*arr)[1:]
	return tmp
}

func addToQueue(musicHome *string, artists *[]string, queue *[]string) {
	var ar int = 1
	var al int = 1
	var so int = 1
	for ar > -1 {
		listArtists(*artists)
		fmt.Print("Select Artist: ")
		fmt.Scan(&ar)
		if ar < 0 {
			ar = 1
			break
		}
		for al > -1 {
			als := listAlbums(*musicHome + "/" + (*artists)[ar])
			fmt.Print("Select Album: ")
			fmt.Scan(&al)
			if al < 0 {
				al = 1
				break
			}
			fmt.Println(*musicHome + "/" + (*artists)[ar] + "/" + als[al])
			for so > -1 {
				trks := listTracks(*musicHome + "/" + (*artists)[ar] + "/" + als[al])
				fmt.Print("Select Songs: ")
				fmt.Scan(&so)
				if so < 0 {
					so = 1
					break
				}
				enqueue(queue, *musicHome+"/"+(*artists)[ar]+"/"+als[al]+"/"+trks[so])
				fmt.Println("Added " + trks[so] + " to the queue")
			}
		}
	}
}

func listQueue(queue *[]string) {

	println()
	println("Queued Songs:")
	println("Position / Aritst / Album / Song Title")
	for i, song := range *queue {
		artist := strings.Split(strings.Split(song, "/")[len(strings.Split(song, "/"))-3], ".")[0]
		album := strings.Split(strings.Split(song, "/")[len(strings.Split(song, "/"))-2], ".")[0]
		title := strings.Split(strings.Split(song, "/")[len(strings.Split(song, "/"))-1], ".")[0]
		println("(", i, ") ", artist, "/", album, "/", title)
	}
	println()

}

func printUI() {
	fmt.Println("Enter Command")
	fmt.Println("(Q)quit")
	fmt.Println("(S)tart queue")
	fmt.Println("(P)lay queue")
	fmt.Println("(L)ist queue")
	fmt.Print("cmd>> ")
}

func main() {

	musicHome, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("Cannot find music directory")
	}

	queue := make([]string, 0)

	musicHome = musicHome + "/Music"

	contents, err := os.ReadDir(musicHome)
	artists := make([]string, 0)

	if err != nil {
		fmt.Println("Cannot read music directory")
	}

	for _, artist := range contents {
		if artist.IsDir() {
			artists = append(artists, artist.Name())
		}
	}

	cmd := ""

	for "q" != cmd && "Q" != cmd {
		printUI()
		fmt.Scan(&cmd)

		switch cmd {
		case "s":
			fallthrough
		case "S":
			addToQueue(&musicHome, &artists, &queue)
		case "l":
			fallthrough
		case "L":
			listQueue(&queue)
		case "p":
			fallthrough
		case "P":
			go func() {
				song := dequeue(&queue)
				for "" != song {
					title := strings.Split(strings.Split(song, "/")[len(strings.Split(song, "/"))-1], ".")[0]
					fmt.Println("\nPlaying: " + title)
					playSong(song)
					song = dequeue(&queue)
					printUI()
				}
			}()
		}
	}
}
